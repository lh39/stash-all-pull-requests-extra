package com.infusion.stash.allpullrequests.contextprovider;

import java.util.Map;

import com.atlassian.bitbucket.project.ProjectService;
import com.atlassian.bitbucket.pull.PullRequestSearchRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.pull.PullRequestState;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;

/**
 * @author lh39
 */
public class PullRequestCountProvider implements ContextProvider {

    private final ProjectService projectService;
    private final RepositoryService repositoryService;
    private final PullRequestService pullRequestService;

    public PullRequestCountProvider(ProjectService projectService,
                                    RepositoryService repositoryService, PullRequestService pullRequestService) {
        this.projectService = projectService;
        this.repositoryService = repositoryService;
        this.pullRequestService = pullRequestService;
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> arg0) {
        long pullRequestCount = this.projectService.findAllKeys().stream().flatMap(project -> {
            PageRequest pageRequest = new PageRequestImpl(0, PageRequest.MAX_PAGE_LIMIT);
            Page<Repository> repositories = this.repositoryService.findByProjectKey(project, pageRequest);
            return repositories.stream();
        }).mapToLong(repository -> {
            PullRequestSearchRequest pullRequestSearchRequest = new PullRequestSearchRequest.Builder()
                    .fromRepositoryId(repository.getId()).state(PullRequestState.OPEN).build();
            return this.pullRequestService.count(pullRequestSearchRequest);
        }).sum();

        arg0.put("pullrequest-count", pullRequestCount);
        return arg0;
    }

    @Override
    public void init(Map<String, String> arg0) throws PluginParseException {
    }
}
